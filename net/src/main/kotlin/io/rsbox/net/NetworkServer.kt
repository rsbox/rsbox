package io.rsbox.net

import io.netty.channel.ChannelFuture
import java.net.InetSocketAddress
import java.util.concurrent.CountDownLatch

/**
 * Represents a networking server.
 *
 * @author Kyle Escobar
 */

abstract class NetworkServer(open val latch: CountDownLatch) {

    abstract fun bind(address: InetSocketAddress): ChannelFuture

    open fun onBindSuccess(address: InetSocketAddress) { latch.countDown() }

    abstract fun onBindFailure(address: InetSocketAddress, t: Throwable)

    abstract fun shutdown()

}