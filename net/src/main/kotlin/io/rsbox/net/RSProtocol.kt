package io.rsbox.net

import io.netty.buffer.ByteBuf
import io.rsbox.net.GameSession

/**
 * Represents a current protocol set for a session.
 *
 * @author Kyle Escobar
 */
abstract class RSProtocol(open val session: GameSession) {

    /**
     * Encode an outgoing packet of data.
     */
    abstract fun encode(msg: Any, out: ByteBuf)

    /**
     * Decode an incoming packet of data.
     */
    abstract fun decode(buf: ByteBuf, out: MutableList<Any>)

    /**
     * Invoked when a packet of data is decoded and needs to be handled.
     */
    abstract fun handle(msg: Any)

    /**
     * Invoked when the session becomes inactive or disconnects.
     */
    abstract fun disconnect()

}
 