package io.rsbox.net

import io.netty.channel.Channel
import io.netty.channel.ChannelHandlerContext
import io.rsbox.config.ServerConfig
import io.rsbox.net.handshake.HandshakeProtocol
import io.rsbox.net.pipeline.GameChannelDecoder
import io.rsbox.net.pipeline.GameChannelEncoder
import io.rsbox.util.Injectable
import org.koin.core.inject
import org.tinylog.kotlin.Logger
import java.net.SocketAddress
import java.util.*

/**
 * Represents a client's stateful connection to the server.
 *
 * @author Kyle Escobar
 */

class GameSession(val ctx: ChannelHandlerContext) : Injectable {

    /**
     * The randomized session id used to identify this session.
     */
    val sessionId = (Math.random() * Int.MAX_VALUE)

    /**
     * The current session channel.
     */
    val channel: Channel get() = ctx.channel()

    /**
     * The IP address of the client this session represents.
     */
    val address = channel.remoteAddress()

    /**
     * The current protocol instance.
     */
    val protocol get() = channel.attr(ChannelAttributes.PROTOCOL)

    /**
     * The randomized seed generated during the LOGIN handshake.
     */
    var seed: Long = (Math.random() * Long.MAX_VALUE).toLong()

    /**
     * The Server config instance.
     */
    private val config: ServerConfig by inject()

    /**
     * Invoked when a session connects to the server.
     */
    internal fun onConnect() {
        /*
         * Build the initial pipeline
         */
        val p = ctx.pipeline()
        p.addBefore("handler", "game_decoder", GameChannelDecoder(this))
        p.addBefore("game_decoder", "game_encoder", GameChannelEncoder(this))

        protocol.set(HandshakeProtocol(this))
    }

    /**
     * Invoked when a session disconnects from the server.
     */
    internal fun onDisconnect() {
        @Suppress("DEPRECATION") val protocol = channel.attr(ChannelAttributes.PROTOCOL).andRemove
        protocol?.disconnect()
        channel.close()
    }

    /**
     * Invoked when a message has been decoded and is awaiting handling.
     */
    internal fun onMessage(msg: Any) {
        protocol.get().handle(msg)
    }

    /**
     * Invoked when an exception is thrown in the channel's thread.
     */
    internal fun onError(cause: Throwable) {
        if(config.developerMode) {
            if(cause.stackTrace.isEmpty() || cause.stackTrace[0].methodName != "read0") {
                Logger.error(cause) { "An exception occurred for [session: $sessionId]" }
            }
        }
        channel.close()
    }
}