package io.rsbox.net.pipeline

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToByteEncoder
import io.rsbox.net.GameSession

/**
 * Responsible for encoding message objects into bytes to be sent over the
 * channel socket.
 *
 * @author Kyle Escobar
 */
class GameChannelEncoder(private val session: GameSession) : MessageToByteEncoder<Any>() {

    override fun encode(ctx: ChannelHandlerContext, msg: Any, out: ByteBuf) {
        session.protocol.get().encode(msg, out)
    }

}