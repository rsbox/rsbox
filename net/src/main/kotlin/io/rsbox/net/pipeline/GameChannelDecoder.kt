package io.rsbox.net.pipeline

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.ByteToMessageDecoder
import io.rsbox.net.GameSession

/**
 * Responsible for decoding inbound bytes into data objects for handling.
 *
 * @author Kyle Escobar
 */
class GameChannelDecoder(private val session: GameSession) : ByteToMessageDecoder() {

    override fun decode(ctx: ChannelHandlerContext, buf: ByteBuf, out: MutableList<Any>) {
        session.protocol.get().decode(buf, out)
    }

}