package io.rsbox.net.pipeline

import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.rsbox.net.GameSession
import java.lang.IllegalStateException
import java.util.concurrent.atomic.AtomicReference

/**
 * Processes events for the an established channel.
 *
 * @author Kyle Escobar
 */

class GameChannelHandler : ChannelInboundHandlerAdapter() {

    /**
     * The associated session with this handler.
     */
    private val session = AtomicReference<GameSession>(null)

    /**
     * Invoked when the channel is established.
     */
    override fun channelActive(ctx: ChannelHandlerContext) {
        val newSession = GameSession(ctx)
        if(!session.compareAndSet(null, newSession)) {
            throw IllegalStateException("Session cannot be set more than once.")
        }
        newSession.onConnect()
    }

    /**
     * Invoked when the channel becomes inactive.
     */
    override fun channelInactive(ctx: ChannelHandlerContext) = session.get().onDisconnect()

    /**
     * Invoked when data arrives inbound from the connected client over the channel.
     */
    override fun channelRead(ctx: ChannelHandlerContext, msg: Any) = session.get().onMessage(msg)

    /**
     * Invoked when an exception is thrown in the channel thread.
     */
    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) = session.get().onError(cause)
}