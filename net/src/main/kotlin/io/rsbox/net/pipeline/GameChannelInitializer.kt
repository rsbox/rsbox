package io.rsbox.net.pipeline

import io.netty.channel.ChannelInitializer
import io.netty.channel.socket.SocketChannel
import io.netty.handler.timeout.IdleStateHandler
import io.netty.handler.traffic.GlobalTrafficShapingHandler
import io.rsbox.util.Injectable
import org.koin.core.inject
import java.util.concurrent.Executors

/**
 * Used to initialized new established channel connections.
 *
 * @author Kyle Escobar
 */

class GameChannelInitializer : ChannelInitializer<SocketChannel>(), Injectable {

    /**
     * The global traffic throttling handler.
     * This is used to cap the bandwidth which the server can utilize.
     *
     * By default, this is set to unlimited. But if you need to reserve network bandwidth for
     * other system services, this may be useful to set.
     */
    private val globalTraffic = GlobalTrafficShapingHandler(Executors.newSingleThreadScheduledExecutor(), 0, 0, 1000)

    /**
     * Initializes the new channel
     *
     * @param ch The new established channel
     */
    override fun initChannel(ch: SocketChannel) {
        /**
         * The timeout handler.
         */
        val timeoutHandler = IdleStateHandler(IDLE_READ_TIMEOUT, IDLE_WRITE_TIMEOUT, 0)

        /**
         * The primary channel handler
         */
        val handler: GameChannelHandler by inject()

        val p = ch.pipeline()

        /*
         * Setup the default pipeline
         */
        p.addLast("global_traffic", globalTraffic)
        p.addLast("timeout", timeoutHandler)
        p.addLast("handler", handler)
    }

    companion object {
        /**
         * The time in seconds which a client is disconnected due to a read timeout
         */
        private const val IDLE_READ_TIMEOUT = 30

        /**
         * The time in seconds which a client is disconnected due to a write timeout
         */
        private const val IDLE_WRITE_TIMEOUT = 30
    }
}