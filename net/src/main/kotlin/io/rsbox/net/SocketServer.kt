package io.rsbox.net

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.Channel
import io.netty.channel.ChannelFuture
import io.netty.channel.ChannelOption
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel
import org.tinylog.kotlin.Logger
import java.net.InetSocketAddress
import java.util.concurrent.CountDownLatch

/**
 * @author Kyle Escobar
 */

abstract class SocketServer(override val latch: CountDownLatch) : NetworkServer(latch) {

    private val bossGroup = NioEventLoopGroup(2)
    private val workerGroup = NioEventLoopGroup(1)

    internal val bootstrap = ServerBootstrap()

    internal lateinit var channel: Channel

    init {
        bootstrap
            .group(bossGroup, workerGroup)
            .channel(NioServerSocketChannel::class.java)
            .childOption(ChannelOption.TCP_NODELAY, true)
            .childOption(ChannelOption.SO_KEEPALIVE, true)
    }

    override fun bind(address: InetSocketAddress): ChannelFuture {
        val future = bootstrap.bind(address).addListener { f ->
            if(f.isSuccess) {
                onBindSuccess(address)
            } else {
                onBindFailure(address, f.cause())
            }
        }

        channel = future.channel()
        return future
    }

    override fun shutdown() {
        Logger.info { "Preparing to shutdown socket server." }

        channel.close()

        bootstrap.config().group().shutdownGracefully()
        bootstrap.config().childGroup().shutdownGracefully()

        try {
            bootstrap.config().group().terminationFuture().sync()
            bootstrap.config().childGroup().terminationFuture().sync()
        } catch (e : InterruptedException) {
            Logger.error(e) { "Socket server shutdown process interrupted!" }
        }
    }
}