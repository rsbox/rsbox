package io.rsbox.net

import io.rsbox.net.pipeline.GameChannelHandler
import io.rsbox.net.pipeline.GameChannelInitializer
import org.koin.dsl.module

/**
 * @author Kyle Escobar
 */
  
val netModule = module {

    single { GameNetworkServer() }
    factory { GameChannelInitializer() }
    factory { GameChannelHandler() }

}