package io.rsbox.net.handshake

import io.netty.buffer.ByteBuf
import io.rsbox.engine.ClientStatusType
import io.rsbox.net.GameSession
import io.rsbox.net.RSProtocol
import io.rsbox.net.js5.JS5Protocol
import io.rsbox.net.login.LoginProtocol

/**
 * Responsible for handling the handshake protocol which is
 * by default set for new channel connections.
 *
 * @author Kyle Escobar
 */
class HandshakeProtocol(override val session: GameSession) : RSProtocol(session) {

    /**
     * Invoked when the session disconnects with this protocol set.
     */
    override fun disconnect() {}

    /**
     * Invoked for inbound messages after being decoded.
     */
    override fun handle(msg: Any) {}

    /**
     * Encodes outgoing packets from data objects to bytes.
     */
    override fun encode(msg: Any, out: ByteBuf) {
        when(msg) {
            is ClientStatusType -> {
                out.writeByte(msg.id)
            }

            else -> throw IllegalStateException("Failed to encode type ${msg::class.java.simpleName} in handshake protocol.")
        }
    }

    /**
     * Encodes incoming packets from bytes to data objects.
     */
    override fun decode(buf: ByteBuf, out: MutableList<Any>) {
        if(!buf.isReadable) return

        val opcode = buf.readByte().toInt()

        when(HandshakeType.values.firstOrNull { it.opcode == opcode }) {
            HandshakeType.JS5 -> session.protocol.set(JS5Protocol(session))
            HandshakeType.LOGIN -> {
                session.protocol.set(LoginProtocol(session))
                /*
                 * Send a byte 0 to signal to the client the handshake was received.
                 * Send the random session seed to the client.
                 */
                session.channel.writeAndFlush(session.ctx.alloc().buffer(1).writeByte(0))
                session.channel.writeAndFlush(session.ctx.alloc().buffer(8).writeLong(session.seed))
            }

            else -> throw IllegalStateException("Unhandled handshake type with opcode $opcode.")
        }
    }

}