package io.rsbox.net

/**
 * @author Kyle Escobar
 */

enum class ProtocolType {

    DESKTOP,

    MOBILE;

}