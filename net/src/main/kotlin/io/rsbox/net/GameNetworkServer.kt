package io.rsbox.net

import io.netty.channel.ChannelFuture
import io.rsbox.net.pipeline.GameChannelInitializer
import io.rsbox.util.Injectable
import org.koin.core.inject
import org.tinylog.kotlin.Logger
import java.net.InetSocketAddress
import java.util.concurrent.CountDownLatch
import kotlin.system.exitProcess

/**
 * Represents the main game server which is responsible for building / handling
 * of the network related tasks.
 *
 * @author Kyle Escobar
 */

class GameNetworkServer : SocketServer(CountDownLatch(3)), Injectable {

    init {
        val initializer: GameChannelInitializer by inject()

        bootstrap.childHandler(initializer)
    }

    /**
     * Binds the networking socket to the [address].
     */
    override fun bind(address: InetSocketAddress): ChannelFuture {
        Logger.info { "Preparing to bind network socket to address [${address.hostString}:${address.port}]." }
        return super.bind(address)
    }

    /**
     * Invoked upon success binding of the socket.
     */
    override fun onBindSuccess(address: InetSocketAddress) {
        Logger.info { "Game network is now listening on [${address.hostString}:${address.port}]." }
        super.onBindSuccess(address)
    }

    /**
     * Invoked upon failure to bind the socket. Throws a cause.
     * If this is invoked, the process is exited with status = 1
     */
    override fun onBindFailure(address: InetSocketAddress, t: Throwable) {
        Logger.error(t) { "Failed to bind network socket to address [${address.hostString}:${address.port}]. "}
        exitProcess(1)
    }


}