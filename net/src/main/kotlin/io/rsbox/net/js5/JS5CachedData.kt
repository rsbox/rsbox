package io.rsbox.net.js5

/**
 * Stores the js5 archive and indexes in memory for faster JS5 loading times.
 *
 * @author Kyle Escobar
 */

object JS5CachedData {

    /**
     * The index file data bytes.
     */
    var INDEXES: ByteArray? = null

}