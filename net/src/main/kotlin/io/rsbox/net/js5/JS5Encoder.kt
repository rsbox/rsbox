package io.rsbox.net.js5

import io.netty.buffer.ByteBuf
import io.rsbox.net.GameSession

/**
 * Responsible for encoding JS5 response packets into bytes to be sent to the
 * server.
 *
 * @author Kyle Escobar
 */
class JS5Encoder(private val session: GameSession) {

    fun encode(msg: JS5Response, out: ByteBuf) {
        out.writeByte(msg.index)
        out.writeShort(msg.archive)

        /**
         * Every 512 bytes, the client reads a placeholder -1. We need
         * to make sure we write a byte value of '-1' every 512 bytes.
         */
        msg.data.forEach { byte ->
            if(out.writerIndex() % 512 == 0) {
                out.writeByte(-1)
            }
            out.writeByte(byte.toInt())
        }
    }

}