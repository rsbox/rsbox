package io.rsbox.net.js5

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelFutureListener
import io.rsbox.engine.ClientStatusType
import io.rsbox.engine.RSEngine
import io.rsbox.net.GameSession
import io.rsbox.util.Injectable
import org.koin.core.inject
import org.tinylog.kotlin.Logger

/**
 * Responsible for decoding JS5 requests from bytes into readable data.
 *
 * This decoder is stateful since its possible for the client to resend the revision
 * request data. This needs to be processed again.
 *
 * @author Kyle Escobar
 */
class JS5Decoder(private val session: GameSession) : Injectable {

    private var state = State.REVISION_REQUEST

    /**
     * These are the cache request opcodes.
     */
    private val REQUEST_PRIORITY = 0
    private val REQUEST_NORMAL = 1
    private val GAME_INIT = 2
    private val GAME_LOADING = 3
    private val GAME_READY = 6

    /**
     * The engine singleton
     */
    private val engine: RSEngine by inject()

    /**
     * Decodes bytes into a [JS5Request] object.
     *
     * @param buf The incoming buffer of bytes.
     */
    fun decode(buf: ByteBuf, out: MutableList<Any>) {
        when(state) {
            State.REVISION_REQUEST -> this.decodeRevisionRequest(buf)
            State.CACHE_REQUEUST -> this.decodeCacheRequest(buf, out)
        }
    }

    /**
     * Decodes a revision request.
     *
     * Reads the revision as an INT.
     *
     * If that revision does not match the engine set revision number,
     * The client status REVISION_MISMATCH is sent and the channel closes.
     *
     * Otherwise the decoding state is set to CACHE_REQUEST and the
     * client status ACCEPTABLE is sent telling the client it is ready to
     * receive the JS5 index and archive requests.
     */
    private fun decodeRevisionRequest(buf: ByteBuf) {
        if(buf.readableBytes() < 4) return

        val revision = buf.readInt()

        if(engine.config.revision != revision) {
            buf.readBytes(buf.readableBytes())
            Logger.info { "Client channel disconnected do to JS5 revision mismatch. [client revision=$revision required=${engine.config.revision}]" }
            session.ctx.writeAndFlush(ClientStatusType.REVISION_MISMATCH).addListener(ChannelFutureListener.CLOSE)
        } else {
            state = State.CACHE_REQUEUST
            session.ctx.writeAndFlush(ClientStatusType.ACCEPTABLE)
        }
    }

    /**
     * Decodes the JS5 cache request.
     */
    private fun decodeCacheRequest(buf: ByteBuf, out: MutableList<Any>) {
        if(!buf.isReadable) return

        buf.markReaderIndex()

        /**
         * Skip the game status opcodes as we do not
         * care when the game is done processing the request.
         *
         * This is used in OSRS bc they utilize a broker that handles JS5
         * separate from the world server the client connects to.
         */
        when(val requestOpcode = buf.readByte().toInt()) {
            GAME_INIT, GAME_LOADING, GAME_READY -> {
                buf.skipBytes(3)
            }

            REQUEST_NORMAL, REQUEST_PRIORITY -> {
                if(buf.readableBytes() >= 3) {
                    val index = buf.readUnsignedByte().toInt()
                    val archive = buf.readUnsignedShort()
                    val priority = (requestOpcode == REQUEST_PRIORITY)

                    val request = JS5Request(index, archive, priority)
                    out.add(request)
                } else {
                    buf.resetReaderIndex()
                }
            }
        }
    }

    private enum class State {
        REVISION_REQUEST,
        CACHE_REQUEUST;
    }
}