package io.rsbox.net.js5

import com.google.common.primitives.Ints
import io.rsbox.engine.RSEngine
import io.rsbox.net.GameSession
import io.rsbox.util.Injectable
import net.runelite.cache.fs.Container
import net.runelite.cache.fs.Store
import net.runelite.cache.fs.jagex.CompressionType
import net.runelite.cache.fs.jagex.DiskStorage
import org.koin.core.inject
import org.tinylog.kotlin.Logger

/**
 * Responsible for the processing of [JS5Request] packets.
 *
 * Reads the request and unpacks the desired index and archive files from the
 * engine's game cache store and sends the bytes as a [JS5Response] packet.
 *
 * @author Kyle Escobar
 */
class JS5Handler(private val session: GameSession) : Injectable {

    private val engine: RSEngine by inject()
    private val cacheStore: Store get() = engine.cacheStore

    /**
     * Handles the request.
     */
    fun handle(request: JS5Request) {
        if(request.index == 255) {
            sendIndexData(request)
        } else {
            sendArchiveData(request)
        }
    }

    /**
     * Loads and sends the index cache file data.
     */
    private fun sendIndexData(request: JS5Request) {
        val data: ByteArray

        if(request.archive == 255) {
            /*
             * If the index data is not in memory, Load it and save
             * it in memory.
             */
            if(JS5CachedData.INDEXES == null) {
                val buf = session.ctx.alloc().heapBuffer(cacheStore.indexes.size * 8)

                cacheStore.indexes.forEach { index ->
                    buf.writeInt(index.crc)
                    buf.writeInt(index.revision)
                }

                val container = Container(CompressionType.NONE, -1)
                container.compress(buf.array().copyOf(buf.readableBytes()), null)

                JS5CachedData.INDEXES = container.data
                buf.release()
            }

            data = JS5CachedData.INDEXES!!
        } else {
            val storage = cacheStore.storage as DiskStorage
            data = storage.readIndex(request.archive)
        }

        val response = JS5Response(request.index, request.archive, data)
        session.ctx.writeAndFlush(response)
    }

    /**
     * Loads and sends the archive cache file data.
     */
    private fun sendArchiveData(request: JS5Request) {
        val index = cacheStore.findIndex(request.index)!!
        val archive = index.getArchive(request.archive)!!
        var data = cacheStore.storage.loadArchive(archive)

        if(data != null) {
            val compression = data[0]
            val length = Ints.fromBytes(data[1], data[2], data[3], data[4])
            val dataLength = length + (if(compression.toInt() != CompressionType.NONE) 9 else 5)

            if(dataLength != length && data.size - dataLength == 2) {
                data = data.copyOf(data.size - 2)
            }

            val response = JS5Response(request.index, request.archive, data)
            session.ctx.writeAndFlush(response)
        } else {
            Logger.warn { "Failed to read corrupted cache data. [index=${request.index} archive=${request.archive}]." }
        }
    }
}