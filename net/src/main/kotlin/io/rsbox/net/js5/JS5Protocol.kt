package io.rsbox.net.js5

import io.netty.buffer.ByteBuf
import io.rsbox.engine.ClientStatusType
import io.rsbox.net.GameSession
import io.rsbox.net.RSProtocol

/**
 * Responsible for handling network communication when the channel protocol attribute is set to this class.
 *
 * @author Kyle Escobar
 */
class JS5Protocol(override val session: GameSession) : RSProtocol(session) {

    private val decoder = JS5Decoder(session)
    private val encoder = JS5Encoder(session)
    private val handler = JS5Handler(session)

    /**
     * Encodes either a [ClientStatusType] or [JS5Response] packet into bytes.
     */
    override fun encode(msg: Any, out: ByteBuf) {
        if(msg is ClientStatusType) {
            out.writeByte(msg.id)
        }
        else if(msg is JS5Response) {
            encoder.encode(msg, out)
        }
    }

    /**
     * Decode a JS5 request into a data object for handling.
     */
    override fun decode(buf: ByteBuf, out: MutableList<Any>) {
        decoder.decode(buf, out)
    }

    /**
     * Handle a message after it has been decoded.
     */
    override fun handle(msg: Any) {
        if(msg is JS5Request) {
            handler.handle(msg)
        }
    }

    /**
     * Invoked when the session disconnects with this protocol set.
     */
    override fun disconnect() {}

}