package io.rsbox.net.login

import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled
import io.netty.channel.ChannelFutureListener
import io.rsbox.engine.ClientStatusType
import io.rsbox.engine.RSEngine
import io.rsbox.net.GameSession
import io.rsbox.util.Injectable
import io.rsbox.util.buffer.readJagexString
import io.rsbox.util.buffer.readString
import io.rsbox.util.decipherXTEA
import org.koin.core.inject
import org.tinylog.kotlin.Logger
import java.math.BigInteger

/**
 * Responsible for decoding login request data bytes.
 *
 * @author Kyle Escobar
 */
class LoginDecoder(private val session: GameSession) : Injectable {

    private var state = State.HANDSHAKE

    /**
     * Login handshake opcodes
     */
    private val LOGIN_NORMAL = 16
    private val LOGIN_RECONNECT = 18

    private var reconnecting = false
    private var length = -1
    private var mobile = false

    private val engine: RSEngine by inject()

    /**
     * Decodes the buffer based on current state.
     */
    fun decode(buf: ByteBuf, out: MutableList<Any>) {
        buf.markReaderIndex()
        when(state) {
            State.HANDSHAKE -> this.decodeHandshake(buf)
            State.REQUEST -> this.decodeLoginRequest(buf, out)
        }
    }

    /**
     * Decodes the login request handshake. This is ued
     * to detect whether a client is reconnecting or sending an initial connection.
     */
    private fun decodeHandshake(buf: ByteBuf) {
        if(!buf.isReadable) return

        when(buf.readByte().toInt()) {

            LOGIN_NORMAL -> {
                state = State.REQUEST
            }

            LOGIN_RECONNECT -> {
                reconnecting = true
                state = State.REQUEST
            }

            else -> this.sendStatus(ClientStatusType.BAD_SESSION_ID)
        }
    }

    /**
     * Decodes information about the request.
     * Also includes a revision check.
     */
    private fun decodeHeader(buf: ByteBuf) {
        if(buf.readableBytes() >= 3) {
            val size = buf.readUnsignedShort()

            if(buf.readableBytes() >= size) {
                val revision = buf.readInt()
                buf.skipBytes(Int.SIZE_BYTES)

                mobile = (buf.readByte().toInt() == 2)

                if(revision != engine.config.revision) {
                    this.sendStatus(ClientStatusType.REVISION_MISMATCH)
                    return
                }

                this.length = size - (Int.SIZE_BYTES + Int.SIZE_BYTES + Byte.SIZE_BYTES)
            }

        } else {
            buf.resetReaderIndex()
        }
    }

    /**
     * Decodes the login request payload.
     */
    private fun decodeLoginRequest(buf: ByteBuf, out: MutableList<Any>) {
        this.decodeHeader(buf)

        if(buf.readableBytes() >= length) {
            buf.markReaderIndex()

            /*
             * Decrypt the buffer using RSA encryption keys.
             */
            val secureBuf = buf.decryptRSA()

            /*
             * Test if the buffer decryption is correct.
             * The client sends a unsigned byte with a value of '1'
             * Check if that is readable.
             */
            val encryptionCheck = secureBuf.readUnsignedByte().toInt()
            if(encryptionCheck != 1) {
                buf.resetReaderIndex()
                buf.skipBytes(length)

                this.sendStatus(ClientStatusType.COULD_NOT_COMPLETE_LOGIN)

                Logger.info { "Login request form [${session.address}] REJECTED due to invalid RSA encryption keys." }
                return
            }

            val xteaKeys = IntArray(4) { secureBuf.readInt() }
            val seed = secureBuf.readLong()

            var mfaCode: Int = -1
            val password: String?
            val lastXteaKeys = IntArray(4)

            if(reconnecting) {
                for(i in 0 until 4) {
                    lastXteaKeys[i] = secureBuf.readInt()
                }

                mfaCode = -1
                password = null
            } else {
                val authOpcode = secureBuf.readByte().toInt()

                when(authOpcode) {
                    1 -> {
                        mfaCode = secureBuf.readUnsignedMedium()
                        secureBuf.skipBytes(Byte.SIZE_BYTES)
                    }

                    2 -> secureBuf.skipBytes(Int.SIZE_BYTES)

                    else -> mfaCode = secureBuf.readInt()
                }

                secureBuf.skipBytes(Byte.SIZE_BYTES)
                password = secureBuf.readString()
            }

            val xteaBuf = when(reconnecting) {
                true -> buf.decipherXTEA(lastXteaKeys)
                false -> buf.decipherXTEA(xteaKeys)
            }

            val username = xteaBuf.readString()

            val clientResizable = (xteaBuf.readByte().toInt() shr 1) == 1
            val clientWidth = xteaBuf.readUnsignedShort()
            val clientHeight = xteaBuf.readUnsignedShort()

            xteaBuf.skipBytes(24) // random.dat data
            xteaBuf.readString()
            xteaBuf.skipBytes(Int.SIZE_BYTES)

            xteaBuf.skipBytes(Byte.SIZE_BYTES * 10)
            xteaBuf.skipBytes(Short.SIZE_BYTES)
            xteaBuf.skipBytes(Byte.SIZE_BYTES)
            xteaBuf.skipBytes(Byte.SIZE_BYTES * 3)
            xteaBuf.skipBytes(Short.SIZE_BYTES)
            xteaBuf.readJagexString()
            xteaBuf.readJagexString()
            xteaBuf.readJagexString()
            xteaBuf.readJagexString()
            xteaBuf.skipBytes(Byte.SIZE_BYTES)
            xteaBuf.skipBytes(Short.SIZE_BYTES)
            xteaBuf.readJagexString()
            xteaBuf.readJagexString()
            xteaBuf.skipBytes(Byte.SIZE_BYTES * 2)
            xteaBuf.skipBytes(Int.SIZE_BYTES * 3)
            xteaBuf.skipBytes(Int.SIZE_BYTES)
            xteaBuf.readJagexString()

            xteaBuf.skipBytes(Int.SIZE_BYTES * 3)

            /*
             * Check the client's current cache CRC checksums to ensure they match the server's.
             */
            val serverCrcs = engine.cacheStore.indexes.map { it.crc }.toIntArray()
            val clientCrcs = IntArray(serverCrcs.size) { xteaBuf.readInt() }

            for(i in 0 until serverCrcs.size) {
                if(i == 16) continue
                if(clientCrcs[1] != serverCrcs[i]) {
                    Logger.info { "Login request from [${session.address}] REJECTED due to cache CRC mismatch. [index=$i, crc=${clientCrcs[i]}]" }

                    buf.resetReaderIndex()
                    buf.skipBytes(length)

                    this.sendStatus(ClientStatusType.REVISION_MISMATCH)
                    return
                }
            }

            println("Login request from [${session.address}] [username=$username, password=$password]")
            this.sendStatus(ClientStatusType.ALREADY_ONLINE) // TEMP

        } else {
            buf.resetReaderIndex()
        }
    }

    private fun sendStatus(status: ClientStatusType, disconnect: Boolean = true) {
        val future = session.ctx.writeAndFlush(session.ctx.alloc().buffer(1).writeByte(status.id))
        if(disconnect) {
            future.addListener(ChannelFutureListener.CLOSE)
        }
    }

    /**
     * Decrypts the current buffer using RSA encryption.
     */
    private fun ByteBuf.decryptRSA(): ByteBuf {
        val length = this.readUnsignedShort()
        val secureBuf = this.readBytes(length)

        val rsaBytes = BigInteger(secureBuf.arrayDirect()).modPow(engine.RSA.exponent, engine.RSA.modulus)
        return Unpooled.wrappedBuffer(rsaBytes.toByteArray())
    }

    /**
     * A solution to a bug in the netty byte buf.
     */
    private fun ByteBuf.arrayDirect(): ByteArray {
        if(this.hasArray()) return this.array()

        val bytes = ByteArray(this.readableBytes())
        this.getBytes(this.readerIndex(), bytes)
        return bytes
    }

    private enum class State {
        HANDSHAKE,
        REQUEST;
    }
}