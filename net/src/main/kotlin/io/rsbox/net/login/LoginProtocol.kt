package io.rsbox.net.login

import io.netty.buffer.ByteBuf
import io.rsbox.engine.ClientStatusType
import io.rsbox.net.GameSession
import io.rsbox.net.RSProtocol

/**
 * Responsible for handling login protocol packets.
 *
 * @author Kyle Escobar
 */

class LoginProtocol(override val session: GameSession) : RSProtocol(session) {

    private val decoder = LoginDecoder(session)

    /**
     * Encodes [ClientStatusType] and login response packets.
     */
    override fun encode(msg: Any, out: ByteBuf) {
        if(msg is ByteBuf) {
            out.writeBytes(msg)
        } else {

        }
    }

    /**
     * Decodes login requests
     */
    override fun decode(buf: ByteBuf, out: MutableList<Any>) {
        decoder.decode(buf, out)
    }

    /**
     * Handles the processing of login request packets.
     */
    override fun handle(msg: Any) {

    }

    /**
     * No action required for this protocol.
     */
    override fun disconnect() {}

}