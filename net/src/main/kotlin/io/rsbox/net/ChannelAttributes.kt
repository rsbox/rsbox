package io.rsbox.net

import io.netty.util.AttributeKey

/**
 * @author Kyle Escobar
 */

object ChannelAttributes {

    val PROTOCOL: AttributeKey<RSProtocol> = AttributeKey.valueOf("protocol")

}