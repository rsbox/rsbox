package io.rsbox.config

import org.koin.dsl.module

/**
 * @author Kyle Escobar
 */

val configModule = module {

    single { ServerConfig() }

}