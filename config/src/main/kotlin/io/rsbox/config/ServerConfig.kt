package io.rsbox.config

import com.uchuhimo.konf.ConfigSpec
import io.rsbox.util.constant.PathConstant

/**
 * The main server.yml settings file for the server.
 *
 * @author Kyle Escobar
 */

class ServerConfig : AbstractConfig<ServerConfig>(
    path = PathConstant.SERVER_CONFIG_FILE,
    spec = Companion
){

    val serverName: String by data.property("server.name")
    val revision: Int by data.property("server.revision")
    val developerMode: Boolean by data.property("server.developer_mode")
    val autoRegisterUsers: Boolean by data.property("server.auto_register_users")

    val address: String by data.property("server.network.address")
    val port: Int by data.property("server.network.port")

    /**
     * The config spec definitions
     */
    companion object : ConfigSpec("server") {

        val server_name by optional("RSBox Server", "name")
        val revision by optional(185, "revision")
        val developer_mode by optional(true, "developer_mode")
        val auto_register_users by optional(true, "auto_register_users")

        val network_address by optional("0.0.0.0", "network.address")
        val network_port by optional(43594, "network.port")

        val start_x by optional(3221, "locations.start.x")
        val start_z by optional(3219, "locations.start.z")
        val start_height by optional(0, "location.start.height")

        val home_x by optional(3221, "locations.home.x")
        val home_z by optional(3219, "locations.home.z")
        val home_height by optional(0, "locations.home.height")
    }
}