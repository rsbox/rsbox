package io.rsbox.config

import com.uchuhimo.konf.Config
import com.uchuhimo.konf.ConfigSpec
import com.uchuhimo.konf.source.yaml
import com.uchuhimo.konf.source.yaml.toYaml
import org.tinylog.kotlin.Logger
import java.io.File

/**
 * @author Kyle Escobar
 */

abstract class AbstractConfig<T>(path: String, private val spec: ConfigSpec) {

    /**
     * The konf config instance.
     */
    var data: Config

    init {
        data = Config { addSpec(spec) }
    }

    val configFile = File(path)

    private var setupLogic: (T) -> Unit = { _ -> }
    private var mapLogic: (T) -> Unit = { _ -> }

    /**
     * Specify initialization logic.
     */
    fun setup(logic: (T) -> Unit) {
        this.setupLogic = logic
    }

    fun map(logic: (T) -> Unit) {
        this.mapLogic = logic
    }

    /**
     * Invoked before loading a config.
     */
    @Suppress("UNCHECKED_CAST")
    private fun init() {
        setupLogic(this as T)

        /*
         * Check if the File exists, if not, try to create one
         * using the defaults.
         */
        if(!configFile.exists()) {
            /*
             * We need to try to save it from the spec defaults.
             */
            try {
                this.save()
            } catch (e : Exception) {
                Logger.error(e) { "An error occurred while creating a default config file." }
                return
            }
        }
    }

    /**
     * Loads the config from the file
     */
    @Suppress("UNCHECKED_CAST")
    fun load() {
        this.init()

        data = Config { addSpec(spec) }.from.yaml.file(configFile)

        this.mapLogic(this as T)
    }

    /**
     * Saves the config from from the settings delegated by this
     * sub type to the [data] config.
     */
    fun save() {
        data.toYaml.toFile(configFile)
    }

}