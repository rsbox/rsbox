package io.rsbox.api

import io.rsbox.api.annotation.EngineMapping
import io.rsbox.api.plugin.PluginManager
import io.rsbox.config.ServerConfig
import io.rsbox.util.Injectable

/**
 * The servers engine which operates the game.
 *
 * @author Kyle Escobar
 */

@EngineMapping("RSEngine")
interface Engine : Injectable {

    /**
     * The server config and its settings.
     */
    val config: ServerConfig

    /**
     * The engine's plugin manager.
     */
    val pluginManager: PluginManager

    /**
     * Starts the game engine
     */
    fun start()

    /**
     * Terminates / initiates a shutdown of the game engine.
     */
    fun shutdown()
}