package io.rsbox.api.annotation

/**
 * This is just for sanity and has no underlying effect during runtime.
 * The annotation's purpose is just to make knowing which engine class implements this
 * easier and more clear within the api source.
 *
 * USAGE:
 * @EngineMapping("RSEngine")
 *
 * This mean the interface is implements by the engine class
 * RSEngine.kt
 *
 * @author Kyle Escobar
 */

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class EngineMapping(val name: String)