package io.rsbox.api.plugin

import io.rsbox.api.Engine
import io.rsbox.util.Injectable
import org.koin.core.inject
import org.tinylog.kotlin.Logger

/**
 * Represents a plugin which extends the RSBox API to interact with
 * the server's game engine.
 *
 * @author Kyle Escobar
 */

abstract class RSPlugin : Injectable {

    /**
     * The [Engine] exposed singleton
     */
    val engine: Engine by inject()

    /**
     * Invoked when this plugin is enabled
     */
    abstract fun onEnable()

    /**
     * Invoked when this plugin is disabled.
     */
    abstract fun onDisable()

    companion object {
        /**
         * The plugin logger instance.
         */
        val logger = Logger
    }
}