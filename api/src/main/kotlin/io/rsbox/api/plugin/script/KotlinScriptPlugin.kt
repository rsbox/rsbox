package io.rsbox.api.plugin.script

import io.rsbox.api.Engine
import io.rsbox.util.Injectable
import org.koin.core.inject
import org.tinylog.kotlin.Logger
import kotlin.script.experimental.annotations.KotlinScript

/**
 * Represents a plugin.kts script which is just an inlined kotlin class and is executed
 * when the plugin is enabled.
 *
 * @author Kyle Escobar
 */

@KotlinScript(
    displayName = "Kotlin Plugin Script",
    fileExtension = "plugin.kts"
)
abstract class KotlinScriptPlugin : Injectable {

    /**
     * The server's [Engine] singleton
     */
    val engine: Engine by inject()

    companion object {
        /**
         * The plugin's logger instance.
         */
        val logger = Logger
    }
}