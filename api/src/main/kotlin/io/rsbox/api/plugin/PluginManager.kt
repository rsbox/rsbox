package io.rsbox.api.plugin

/**
 * Represents the manager for engine plugins. Exposes methods for enabling / disabling / reloading plugins.
 *
 * @author Kyle Escobar
 */

interface PluginManager {

    /**
     * Gets the number of loaded plugins.
     */
    fun count(): Int

    /**
     * Gets the number of enabled plugins.
     */
    fun countEnabled(): Int

    /**
     * Gets the number of disabled plugins.
     */
    fun countDisabled(): Int

    /**
     * Enables a plugin by name.
     *
     * @param name The name of the plugin.
     */
    fun enablePlugin(name: String)

    /**
     * Disables a plugin by name.
     *
     * @param name The name of the plugin.
     */
    fun disablePlugin(name: String)

    /**
     * Gets a plugin's instance by name if its enabled.
     *
     * @param name The plugin name.
     *
     * @return [RSPlugin] The plugin's instance. Returns null if plugin is not enabled.
     */
    fun <T : RSPlugin> getPlugin(name: String): T?
}