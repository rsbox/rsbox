package io.rsbox.api.plugin

/**
 * An annotation which is required to be applied to a RSPlugin class which holds information
 * about the plugin including name, version, authors, etc.
 *
 * @author Kyle Escobar
 */

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class PluginInfo(
    val name: String,
    val version: String = "1.0",
    val author: String = "",
    val dependsOn: String = ""
)
 