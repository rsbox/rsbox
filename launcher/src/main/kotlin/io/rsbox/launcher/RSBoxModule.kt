package io.rsbox.launcher

import io.rsbox.config.configModule
import io.rsbox.engine.engineModule
import io.rsbox.net.netModule

/**
 * Contains definitions of modules
 *
 * @author Kyle Escobar
 */

class RSBoxModule {

    /**
     * The global RSBox modules list.
     */
    internal val globalModules = listOf(
        engineModule,
        configModule,
        netModule
    )

}