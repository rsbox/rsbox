package io.rsbox.launcher

import io.rsbox.config.ServerConfig
import io.rsbox.engine.RSEngine
import io.rsbox.net.GameNetworkServer
import io.rsbox.util.Injectable
import io.rsbox.util.constant.PathConstant
import org.koin.core.context.startKoin
import org.koin.core.get
import org.koin.core.inject
import org.tinylog.kotlin.Logger
import java.io.File
import java.net.InetSocketAddress

/**
 * @author Kyle Escobar
 */

object RSBox : Injectable {
    /**
     * The class with module definitions for the dependency injector.
     */
    private val rsboxModule = RSBoxModule()

    @JvmStatic
    fun main(args: Array<String>) {
        println("Initializing...")

        this.checkDirectories()

        Logger.info { "Starting dependency injector." }

        this.startInjector()

        Logger.info { "Dependency injector is running." }

        this.initEngine()

        this.initNetwork()

    }

    /**
     * Checks to make sure all of the required directories have been created.
     * If they don't exist, create them.
     */
    private fun checkDirectories() {
        val paths = arrayOf(
            PathConstant.DATA_DIR,
            PathConstant.CACHE_DIR,
            PathConstant.CONFIG_DIR,
            PathConstant.LOG_DIR,
            PathConstant.PLUGIN_DIR,
            PathConstant.RSA_DIR,
            PathConstant.SAVE_DIR,
            PathConstant.XTEA_DIR
        )

        paths.forEach { path ->
            val f = File(path)
            if(!f.exists()) {
                f.mkdirs()
            }
        }
    }

    /**
     * Starts the koin dependency injector.
     */
    private fun startInjector() {
        startKoin {
            modules(rsboxModule.globalModules)
        }
    }

    /**
     * Initializes the game engine.
     */
    private fun initEngine() {
        val engine: RSEngine = get()
        engine.init()
    }

    /**
     * Initializes the game network server and starts it.
     */
    private fun initNetwork() {
        /*
         * We need to get the address to bind to from the
         * server config.
         */
        val serverConfig: ServerConfig by inject()
        val address = InetSocketAddress(serverConfig.address, serverConfig.port)

        val gameNetworkServer: GameNetworkServer by inject()
        gameNetworkServer.bind(address)
    }
}