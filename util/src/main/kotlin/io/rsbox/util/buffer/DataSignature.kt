package io.rsbox.util.buffer

/**
 * @author Tom <rspsmods@gmail.com>
 */
enum class DataSignature {
    UNSIGNED,
    SIGNED
}