package io.rsbox.util.constant

/**
 * A static declaration of directory / file paths used for this engine.
 *
 * @author Kyle Escobar
 */

object PathConstant {

    const val DATA_DIR = "data/"

    const val CONFIG_DIR = "${DATA_DIR}config/"

    const val LOG_DIR = "${DATA_DIR}log/"

    const val PLUGIN_DIR = "${DATA_DIR}plugins/"

    const val CACHE_DIR = "${DATA_DIR}cache/"

    const val XTEA_DIR = "${DATA_DIR}xteas/"

    const val RSA_DIR = "${DATA_DIR}rsa/"

    const val SAVE_DIR = "${DATA_DIR}saves/"

    /**
     * FILE PATHS
     */

    const val SERVER_CONFIG_FILE = "${CONFIG_DIR}server.yml"

    const val PACKETS_DESKTOP_FILE = "/packets.desktop.yml"

    const val PACKETS_MOBILE_FILE = "/packets.mobile.yml"

    const val RSA_PUBLIC_FILE = "${RSA_DIR}public.key"

    const val RSA_PRIVATE_FILE = "${RSA_DIR}private.key"

    const val RSA_MODULUS_FILE = "${RSA_DIR}modulus.txt"
}