package io.rsbox.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory

/**
 * @author Kyle Escobar
 */

object YamlUtils {

    /**
     * Converts a yaml string to a JSON string.
     */
    fun yamlToJson(yaml: String): String {
        val yamlRader = ObjectMapper(YAMLFactory())
        val obj = yamlRader.readValue(yaml, Object::class.java)

        val jsonWriter = ObjectMapper()
        return jsonWriter.writeValueAsString(obj)
    }

}