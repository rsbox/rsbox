package io.rsbox.util

import org.koin.core.KoinComponent

/**
 * An alias interface for Koin components
 *
 * @author Kyle Escobar
 */

interface Injectable : KoinComponent