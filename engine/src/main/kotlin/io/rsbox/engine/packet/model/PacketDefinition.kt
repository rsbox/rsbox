package io.rsbox.engine.packet.model

import io.rsbox.util.buffer.PacketType

/**
 * @author Kyle Escobar
 */
  
data class PacketDefinition(
    val packet: String,
    val codec: String,
    val handler: String? = null,
    val opcode: Int,
    val type: PacketType,
    val length: Int = 0,
    val data: Array<PacketDefinitionData>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PacketDefinition

        if (packet != other.packet) return false
        if (codec != other.codec) return false
        if (opcode != other.opcode) return false
        if (type != other.type) return false
        if (length != other.length) return false
        if (!data.contentEquals(other.data)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = packet.hashCode()
        result = 31 * result + codec.hashCode()
        result = 31 * result + opcode
        result = 31 * result + type.hashCode()
        result = 31 * result + length
        result = 31 * result + data.contentHashCode()
        return result
    }
}