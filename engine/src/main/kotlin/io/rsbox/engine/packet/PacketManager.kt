package io.rsbox.engine.packet

import com.google.gson.Gson
import io.rsbox.engine.packet.model.*
import io.rsbox.util.YamlUtils
import io.rsbox.util.constant.PathConstant
import org.tinylog.kotlin.Logger
import kotlin.reflect.KClass

/**
 * Responsible for storing multiple revision protocols of packets
 * loaded from the jar resources. This is to provide support for both mobile and
 * desktop connections.
 *
 * @author Kyle Escobar
 */

class PacketManager {

    /**
     * The packet registry for desktop packets.
     */
    private val desktopRegistry = PacketRegistry(RegistryType.DESKTOP)

    /**
     * The registry for mobile packets.
     */
    private val mobileRegistry = PacketRegistry(RegistryType.MOBILE)

    /**
     * Initialize the manager.
     */
    fun init() {
        Logger.info { "Preparing to load packet definitions." }
        /*
         * Load packets from resource files
         */
        this.loadPackets(PathConstant.PACKETS_DESKTOP_FILE, RegistryType.DESKTOP)
        this.loadPackets(PathConstant.PACKETS_MOBILE_FILE, RegistryType.MOBILE)
    }

    /**
     * Loads packet definitions from a filepath from embeded yml resource file.
     */
    @Suppress("UNCHECKED_CAST")
    private fun loadPackets(filePath: String, type: RegistryType) {
        val contents = javaClass.getResource(filePath).readText(Charsets.UTF_8)

        /*
         * Convert the contents to JSON
         */
        val jsonString = YamlUtils.yamlToJson(contents)

        /*
         * Decode into objects and register each entry to the registry type.
         */
        val data = Gson().fromJson(jsonString, PacketDefinitionFile::class.java)

        val registry = when(type) {
            RegistryType.DESKTOP -> desktopRegistry
            RegistryType.MOBILE -> mobileRegistry
        }

        if(data.inbound != null) {
            data.inbound.forEach { def ->
                val codec = Class.forName(def.codec).getDeclaredConstructor().newInstance()
                val handler = Class.forName(def.handler).getDeclaredConstructor().newInstance()

                registry.addInboundDefinition(def)
                registry.registerInbound(def.opcode, codec as InboundCodec<*>, handler as PacketHandler<*>)
            }
        }

        if(data.outbound != null) {
            data.outbound.forEach { def ->
                val packet = Class.forName(def.packet).kotlin
                val codec = Class.forName(def.codec).getDeclaredConstructor().newInstance()

                registry.addOutboundDefinition(def)
                registry.registerOutbound(packet, codec as OutboundCodec<*>)
            }
        }

        Logger.info { "Registered ${registry.inboundDefinitions.size} inbound $type packets." }
        Logger.info { "Registered ${registry.outboundDefinitions.size} outbound $type packets." }
    }
}