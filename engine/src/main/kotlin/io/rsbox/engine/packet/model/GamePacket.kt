package io.rsbox.engine.packet.model

/**
 * Represents a packet which is used during the in-game communication
 * to / from the server.
 *
 * @author Kyle Escobar
 */

interface GamePacket