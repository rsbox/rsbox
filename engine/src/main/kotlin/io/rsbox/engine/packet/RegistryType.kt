package io.rsbox.engine.packet

/**
 * The type of registries storing packets.
 *
 * @author Kyle Escobar
 */

enum class RegistryType {

    DESKTOP,
    MOBILE

}