package io.rsbox.engine.packet.model

/**
 * @author Kyle Escobar
 */
  
data class PacketDefinitionFile(
    val inbound: Array<PacketDefinition>?,
    val outbound: Array<PacketDefinition>?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PacketDefinitionFile

        if (inbound != null) {
            if (other.inbound == null) return false
            if (!inbound.contentEquals(other.inbound)) return false
        } else if (other.inbound != null) return false
        if (outbound != null) {
            if (other.outbound == null) return false
            if (!outbound.contentEquals(other.outbound)) return false
        } else if (other.outbound != null) return false

        return true
    }

    override fun hashCode(): Int {
        var result = inbound?.contentHashCode() ?: 0
        result = 31 * result + (outbound?.contentHashCode() ?: 0)
        return result
    }
}