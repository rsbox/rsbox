package io.rsbox.engine.packet.model

import io.rsbox.util.buffer.DataOrder
import io.rsbox.util.buffer.DataSignature
import io.rsbox.util.buffer.DataTransformation
import io.rsbox.util.buffer.DataType

/**
 * @author Kyle Escobar
 */
  
data class PacketDefinitionData(
    val name: String,
    val type: DataType = DataType.BYTE,
    val order: DataOrder = DataOrder.BIG,
    val trans: DataTransformation = DataTransformation.NONE,
    val sign: DataSignature = DataSignature.UNSIGNED
)