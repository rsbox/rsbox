package io.rsbox.engine.packet

import io.rsbox.engine.packet.model.*
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap
import kotlin.reflect.KClass

/**
 * Stores definitions of packets loaded from a file.
 *
 * @author Kyle Escobar
 */

class PacketRegistry(val type: RegistryType) {

    private val outbound = Object2ObjectOpenHashMap<KClass<*>, OutboundCodec<*>>()
    private val inbound = arrayOfNulls<InboundCodec<out GamePacket>>(256)
    private val handlers = arrayOfNulls<PacketHandler<out GamePacket>>(256)

    internal val inboundDefinitions = mutableListOf<PacketDefinition>()
    internal val outboundDefinitions = mutableListOf<PacketDefinition>()

    internal fun registerInbound(opcode: Int, codec: InboundCodec<*>, handler: PacketHandler<*>) {
        inbound[opcode] = codec
        handlers[opcode] = handler
    }

    internal fun registerOutbound(packet: KClass<*>, codec: OutboundCodec<*>) {
        outbound[packet] = codec
    }

    internal fun addInboundDefinition(def: PacketDefinition) {
        inboundDefinitions.add(def)
    }

    internal fun addOutboundDefinition(def: PacketDefinition) {
        outboundDefinitions.add(def)
    }
}