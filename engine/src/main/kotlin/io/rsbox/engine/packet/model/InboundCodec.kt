package io.rsbox.engine.packet.model

import io.rsbox.engine.packet.PacketRegistry

/**
 * Represents a codec used for decoding game packets from bytes into data objects.
 *
 * @author Kyle Escobar
 */

abstract class InboundCodec<T : GamePacket>(val registry: PacketRegistry) {

}