package io.rsbox.engine.plugin

import io.rsbox.api.plugin.RSPlugin
import io.rsbox.api.plugin.script.KotlinScriptPlugin
import kotlin.reflect.KClass

/**
 * Represents a RSPlugin and a list of script classes that are attached.
 *
 * @author Kyle Escobar
 */

data class Plugin(
    val name: String,
    val version: String,
    val author: String,
    var enabled: Boolean,
    val mainClass: KClass<out RSPlugin>,
    var pluginInstance: RSPlugin? = null,
    val scripts: HashSet<KClass<out KotlinScriptPlugin>>
)