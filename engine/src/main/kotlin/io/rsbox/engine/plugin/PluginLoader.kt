@file:Suppress("PrivatePropertyName")

package io.rsbox.engine.plugin

import io.rsbox.util.constant.PathConstant
import java.io.File
import java.net.URL
import java.net.URLClassLoader
import java.util.jar.JarFile
import kotlin.reflect.KClass

/**
 * Responsible for loading plugins from the data folder.
 *
 * @author Kyle Escobar
 */

class PluginLoader {

    /**
     * An alias plugin directory.
     */
    private val PLUGIN_DIR = PathConstant.PLUGIN_DIR

    /**
     * Gets an array of jar [File]s in the [PLUGIN_DIR] directory.
     */
    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    internal fun getJars(): Array<File> {
        return File(PLUGIN_DIR).listFiles().filter { it.extension == "jar" }.toTypedArray()
    }

    /**
     * Unpacks the classes from a jar file and its contained classes.
     */
    internal fun unpackJar(file: File): HashMap<String, KClass<*>> {
        val jarFile = JarFile(file.path)
        val entries = jarFile.entries()

        val urls = Array(1) { URL("jar:file:${file.path}!/") }
        val classLoader = URLClassLoader.newInstance(urls)

        val classMap = hashMapOf<String, KClass<*>>()

        while(entries.hasMoreElements()) {
            val jarEntry = entries.nextElement()

            /*
             * We want to skip any files which are not .class or are directories.
             */
            if(jarEntry.isDirectory || !jarEntry.name.endsWith(".class")) {
                continue
            }

            /*
             * We want the class name without .class on the end.
             */
            var className = jarEntry.name.substring(0, jarEntry.name.length-6)
            className = className.replace("/", ".")

            val clazz = classLoader.loadClass(className)

            classMap[className] = clazz.kotlin
        }

        classLoader.close()
        jarFile.close()

        return classMap
    }

}