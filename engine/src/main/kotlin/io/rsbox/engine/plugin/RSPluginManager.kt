package io.rsbox.engine.plugin

import io.rsbox.api.plugin.PluginInfo
import io.rsbox.api.plugin.PluginManager
import io.rsbox.api.plugin.RSPlugin
import io.rsbox.api.plugin.script.KotlinScriptPlugin
import io.rsbox.util.Injectable
import org.koin.core.inject
import org.tinylog.kotlin.Logger
import kotlin.reflect.KClass

/**
 * Responsible for storing / loading plugins.
 *
 * @author Kyle Escobar
 */

class RSPluginManager : PluginManager, Injectable {

    /**
     * The plugin storage in memory.
     */
    private val plugins = mutableListOf<Plugin>()

    private val loader: PluginLoader by inject()

    /**
     * Get the number of loaded plugins.
     */
    override fun count(): Int = plugins.size

    /**
     * Get the number of enabled plugins
     */
    override fun countEnabled(): Int = plugins.filter { it.enabled }.size

    /**
     * Get the number of disabled plugins.
     */
    override fun countDisabled(): Int = plugins.filter { !it.enabled }.size

    /**
     * Loads the plugin jars from the plugin directory.
     * If [reload] is true, the current map of plugins are disabled
     * and cleared from memory.
     */
    @Suppress("UNCHECKED_CAST")
    fun loadPluginJars(reload: Boolean = false) {
        Logger.info { "Preparing to load/reload plugin directory." }

        if(reload) {
            Logger.info { "Reloading all plugins." }
            plugins.filter { it.enabled }.forEach { plugin ->
                this.disablePlugin(plugin.name)
            }

            plugins.clear()
        }

        val pluginJars = loader.getJars()

        pluginJars.forEach { jar ->
            val jarClasses = loader.unpackJar(jar)
            val name: String
            val version: String
            val author: String
            var mainClass: KClass<*>? = null
            val scripts = hashSetOf<KClass<out KotlinScriptPlugin>>()

            /**
             * Find the mainClass
             */
            jarClasses.forEach { (_, kClass) ->
                if(kClass.java.superclass.simpleName == "RSPlugin") {
                    mainClass = kClass
                }
            }

            /**
             * Extract annotation info
             */
            val pluginInfo = mainClass!!.java.getDeclaredAnnotation(PluginInfo::class.java)
            name = pluginInfo.name
            version = pluginInfo.version
            author = pluginInfo.author

            /**
             * Find scripts within the plugin.
             */
            jarClasses.forEach { (_, kClass) ->
                if(kClass.java.superclass.simpleName == "KotlinScriptPlugin") {
                    scripts.add(kClass as KClass<out KotlinScriptPlugin>)
                }
            }

            @Suppress("UNCHECKED_CAST") val plugin = Plugin(
                name = name,
                version = version,
                author = author,
                enabled = false,
                mainClass = mainClass!! as KClass<out RSPlugin>,
                pluginInstance = null,
                scripts = scripts
            )

            plugins.add(plugin)

            Logger.info { "Loaded plugin '$name - v$version' with ${scripts.size} scripts." }
        }
    }

    /**
     * Gets a plugin by name.
     */
    @Suppress("UNCHECKED_CAST")
    override fun <T : RSPlugin> getPlugin(name: String): T? {
        /*
         * Check if plugin is found and enabled.
         */
        if(plugins.firstOrNull { it.name == name && it.enabled } == null) {
            return null
        }

        return plugins.first { it.name == name } as T
    }

    /**
     * Enables a plugin and invokes its enabler logic.
     *
     * @param name The name of the plugin.
     */
    override fun enablePlugin(name: String) {
        /*
         * Check if plugin is loaded and already enabled. If so throw error.
         */
        if(plugins.firstOrNull { it.name == name && it.enabled } != null) {
            Logger.warn { "Failed to enable plugin $name. Plugin not found or is already enabled." }
            return
        }

        val plugin = plugins.first { it.name == name }
        val inst = plugin.mainClass.java.getDeclaredConstructor().newInstance()

        plugin.pluginInstance = inst

        /*
         * Execute all scripts.
         */
        plugin.scripts.forEach { script ->
            script.java.getDeclaredConstructor().newInstance()
        }

        inst.onEnable()
        plugin.enabled = true

        Logger.info { "Enabled plugin '${plugin.name} - v${plugin.version}'." }
    }

    /**
     * Disables a plugin and invokes its disabler logic.
     *
     * @param name The name of the plugin.
     */
    override fun disablePlugin(name: String) {
        /*
         * Check if plugin is loaded and is not enabled. If so throw error.
         */
        if(plugins.firstOrNull { it.name == name && !it.enabled } != null) {
            Logger.warn { "Failed to disable plugin $name. Plugin not found or is already disabled." }
            return
        }

        val plugin = plugins.first { it.name == name }
        val inst = plugin.pluginInstance

        inst!!.onDisable()
        plugin.enabled = false

        Logger.info { "Disabled plugin '${plugin.name} - v${plugin.version}'." }
    }

    /**
     * Gets all of the loaded plugins.
     */
    internal fun getPlugins(): List<Plugin> = plugins
}