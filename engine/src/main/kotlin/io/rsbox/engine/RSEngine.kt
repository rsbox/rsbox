package io.rsbox.engine

import com.google.common.base.Stopwatch
import io.rsbox.api.Engine
import io.rsbox.config.ServerConfig
import io.rsbox.engine.module.RSAModule
import io.rsbox.engine.packet.PacketManager
import io.rsbox.engine.plugin.RSPluginManager
import io.rsbox.util.constant.PathConstant
import net.runelite.cache.fs.Store
import org.koin.core.inject
import org.tinylog.kotlin.Logger
import java.io.File
import java.util.concurrent.TimeUnit
import kotlin.system.exitProcess

/**
 * Represents the main game engine which handles the logic for the server.
 *
 * @author Kyle Escobar
 */

class RSEngine : Engine {

    override val config: ServerConfig by inject()

    override val pluginManager: RSPluginManager by inject()

    /**
     * The packet manager singleton
     */
    val packetManager: PacketManager by inject()

    /**
     * The RSA Module singleton
     */
    val RSA: RSAModule by inject()

    /**
     * The cache store.
     */
    val cacheStore = Store(File(PathConstant.CACHE_DIR))

    /**
     * Initializes the engine before starting.
     */
    fun init() {
        Logger.info { "Initializing the game engine." }

        /*
         * Load the server config / create default.
         */
        config.load()

        Logger.info { "Finished game engine initialization." }

        /*
         * Test the config loaded correct settings.
         */
        Logger.info { "${config.serverName} is running OSRS revision ${config.revision}." }

        this.start()
    }

    override fun start() {
        val stopwatch = Stopwatch.createStarted()
        Logger.info { "Game engine is starting..." }

        // Load the cache store
        this.loadCacheStore()

        // Load RSA keys.
        this.loadRSAKeys()

        // Load the packet definitions
        packetManager.init()

        // Load and enable plugins
        pluginManager.loadPluginJars()
        pluginManager.getPlugins().forEach { plugin ->
            pluginManager.enablePlugin(plugin.name)
        }

        stopwatch.stop()

        Logger.info { "Game engine is now running. Startup took ${stopwatch.elapsed(TimeUnit.MILLISECONDS)}ms." }
    }

    override fun shutdown() {
        Logger.info { "Shutting down game engine." }
    }

    /**
     * Loads the cache store and checks if there is a valid cache.
     */
    private fun loadCacheStore() {
        Logger.info { "Loading game cache store." }

        cacheStore.load()

        if(cacheStore.indexes.size > 0) {
            Logger.info { "Game cache store has been loaded from ${PathConstant.CONFIG_DIR}." }
        } else {
            Logger.error { "Failed to load game cache store. Make sure valid cache files are located in '${PathConstant.CACHE_DIR}'." }
            exitProcess(0)
        }
    }

    /**
     * Loads the RSA encryption keys. If none exist, a new key-pair is
     * generated and saved.
     */
    private fun loadRSAKeys() {
        Logger.info { "Loading RSA encryption keys." }
        RSA.loadKeys()
    }
}