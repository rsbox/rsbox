package io.rsbox.engine.module

import io.rsbox.util.constant.PathConstant
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.util.io.pem.PemObject
import org.bouncycastle.util.io.pem.PemReader
import org.bouncycastle.util.io.pem.PemWriter
import org.tinylog.kotlin.Logger
import java.math.BigInteger
import java.nio.file.Files
import java.nio.file.Paths
import java.security.KeyFactory
import java.security.KeyPairGenerator
import java.security.Security
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.security.spec.PKCS8EncodedKeySpec
import kotlin.Exception

/**
 * Responsible for generating and loading RSA cipher keys for encrypting buffers
 * from the client.
 *
 * @author Kyle Escobar
 */

class RSAModule {

    /**
     * The RSA Exponent
     */
    lateinit var exponent: BigInteger

    /**
     * The RSA Modulus
     */
    lateinit var modulus: BigInteger

    private val publicKeyPath = Paths.get(PathConstant.RSA_PUBLIC_FILE)
    private val privateKeyPath = Paths.get(PathConstant.RSA_PRIVATE_FILE)
    private val modulusPath = Paths.get(PathConstant.RSA_MODULUS_FILE)

    /**
     * Loads the keys from the private key file.
     */
    fun loadKeys() {
        if(!Files.exists(publicKeyPath) || !Files.exists(privateKeyPath)) {
            Logger.info { "RSA public / private keys were not found." }
            this.generateKeys()
        }

        try {
            PemReader(Files.newBufferedReader(privateKeyPath)).use { reader ->
                val pem = reader.readPemObject()
                val keySpec = PKCS8EncodedKeySpec(pem.content)

                Security.addProvider(BouncyCastleProvider())

                val factory = KeyFactory.getInstance("RSA", "BC")
                val key = factory.generatePrivate(keySpec) as RSAPrivateKey

                this.exponent = key.privateExponent
                this.modulus = key.modulus
            }
        } catch(e : Exception) {
            Logger.error(e) { "An exception was thrown while reading RSA encryption keys from disk." }
        }
    }

    /**
     * Generates a new private / public key-pair.
     */
    internal fun generateKeys() {
        Logger.info { "Preparing to generate new RSA private/public key-pair." }

        Security.addProvider(BouncyCastleProvider())

        val generator = KeyPairGenerator.getInstance("RSA", "BC")
        generator.initialize(2048)

        val keyPair = generator.generateKeyPair()

        val publicKey = keyPair.public as RSAPublicKey
        val privateKey = keyPair.private as RSAPrivateKey

        println("")
        println("Below is the new generated RSA key. A copy of the modulus has been saved in '${PathConstant.RSA_MODULUS_FILE}'.")
        println("------------------------------------------------------------")
        println("Public Key: ${publicKey.publicExponent.toString(16)}")
        println("Modulus: ${publicKey.modulus.toString(16)}")
        println("")

        try {
            PemWriter(Files.newBufferedWriter(publicKeyPath)).use { writer ->
                writer.writeObject(PemObject("RSA PUBLIC KEY", publicKey.encoded))
            }

            PemWriter(Files.newBufferedWriter(privateKeyPath)).use { writer ->
                writer.writeObject(PemObject("RSA PRIVATE KEY", privateKey.encoded))
            }

            Files.newBufferedWriter(modulusPath).use { writer ->
                writer.write(publicKey.modulus.toString(16))
            }
        } catch(e : Exception) {
            Logger.error(e) { "An exception was thrown while writing RSA keys to disk." }
        }
    }
}