package io.rsbox.engine

import io.rsbox.api.Engine
import io.rsbox.api.plugin.PluginManager
import io.rsbox.engine.module.RSAModule
import io.rsbox.engine.packet.PacketManager
import io.rsbox.engine.plugin.PluginLoader
import io.rsbox.engine.plugin.RSPluginManager
import org.koin.dsl.bind
import org.koin.dsl.module

/**
 * @author Kyle Escobar
 */

val engineModule = module {

    single { RSEngine() } bind(Engine::class)
    single { RSPluginManager() } bind(PluginManager::class)
    factory { PluginLoader() }
    single { PacketManager() }
    single { RSAModule() }
}